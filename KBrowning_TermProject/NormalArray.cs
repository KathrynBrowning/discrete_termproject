﻿using System;
using System.Linq;


namespace KBrowning_TermProject
{
    public class NormalArray
    {

        public int[] GenerateArrayWithRandomNumber(int userSelectedNumber)
        {
            var randomNumber = new Random();
            var normalArray = Enumerable.Repeat(0, userSelectedNumber).Select(i => randomNumber.Next()).ToArray();
            return normalArray;
        }
    }
}
