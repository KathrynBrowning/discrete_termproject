﻿namespace KBrowning_TermProject
{
    /// <summary>
    ///  This class runs the application.
    /// </summary>
    public class Program
    {

        /// <summary>
        ///     Starting point of application.
        /// </summary>
        /// <param name="args"> Not used. </param>
        public static void Main(string[] args)
        {
            var userInterface = new Ui();
            userInterface.Run();
        }
    }
}
