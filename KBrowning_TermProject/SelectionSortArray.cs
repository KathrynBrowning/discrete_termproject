﻿
namespace KBrowning_TermProject
{
    /// <summary>
    ///     This class computes the Selection Sort Array.
    /// </summary>
    public class SelectionSortArray
    {

        /// <summary>
        ///     Sorts the array with a selection sorting algorithm.
        /// </summary>
        /// <param name="array"> The normal array. </param>
        /// <precondition> array != null </precondition>
        /// <postcondition> Array has been sorted. </postcondition>
        /// <returns> The sorted array. </returns>
        public int[] SelectionSortedArray(int[] array)
        {
            int i;
            for (i = 0; i < array.Length - 1; i++)
            {
                var min = i;

                int j;
                for (j = i+1; j < array.Length; j++)
                {
                    if (array[j] < array[min])
                    {
                        min = j;
                    }
                }

                if (min != i)
                {
                    var temp = array[i];
                    array[i] = array[min];
                    array[min] = temp;
                }
            }
            return array;
        }
    }
}
