﻿using System;
using System.Collections.Generic;

namespace KBrowning_TermProject
{
    /// <summary>
    ///     This class organizes and presents the data to the console application.
    /// </summary>
    public class Ui
    {
        private readonly NormalArray normalArray;
        private readonly BubbleSortArray bubbleArray;
        private readonly SelectionSortArray selectionArray;
        private int[] unsortedArray;
        private int enteredNumber;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Ui"/> class.
        /// </summary>
        public Ui()
        {
            this.normalArray = new NormalArray();
            this.bubbleArray = new BubbleSortArray();
            this.selectionArray = new SelectionSortArray();
        }


        /// <summary>
        ///     Runs this instance.
        /// </summary>
        public void Run()
        {
            while (true)
            {
                this.bubbleSort();
                this.resetValues();
                this.selectionSort();

                Console.ReadLine();

                break;
            }
        }

        private void initialInput()
        {
            while (true)
            {
                Console.WriteLine("\nPlease enter a number:");
                this.enteredNumber = Convert.ToInt32(Console.ReadLine());
                if (this.enteredNumber >= 1)
                {
                    return;
                }
                Console.Write("Please enter a positive number greater than 0.\nPress a key to restart.\n");
                Console.ReadLine();
            }
        }

        private void displayNormalArray()
        {
            Console.WriteLine("\nRandom array of " + this.enteredNumber + " numbers:");
            this.unsortedArray = this.normalArray.GenerateArrayWithRandomNumber(this.enteredNumber);
            displayArray(this.unsortedArray);
        }

        private void displayBubbleArray()
        {
            displayArrayHeader("Bubble");
            var bubbleSortedArray = this.bubbleArray.BubbleSortedArray(this.unsortedArray);
            displayArray(bubbleSortedArray);
        }

        private void displaySelectionArray()
        {
            displayArrayHeader("Selection");
            var selectionSortedArray = this.selectionArray.SelectionSortedArray(this.unsortedArray);
            displayArray(selectionSortedArray);
        }

        private static void displayArrayHeader(string arrayType)
        {
            Console.WriteLine("\nPress a key to see the array sorted using a " + arrayType + " Sort");
            Console.ReadLine();
            Console.WriteLine(arrayType + " Sort:");
        }

        private void bubbleSort()
        {
            this.initialInput();
            this.displayNormalArray();
            this.displayBubbleArray();
        }

        private void resetValues()
        {
            this.unsortedArray = null;
            this.enteredNumber = 0;
        }

        private void selectionSort()
        {
            this.initialInput();
            this.displayNormalArray();
            this.displaySelectionArray();
        }

        private static void displayArray(IEnumerable<int> array)
        {
            foreach (var number in array)
            {
                Console.WriteLine(number);
            }
        }
    }
}
